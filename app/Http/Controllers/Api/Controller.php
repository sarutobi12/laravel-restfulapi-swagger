<?php

namespace App\Http\Controllers\Api\V1\Admin;

class Controller
{
    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Laravel API TEST",
     *      description="Laravel API TEST",
     *      @OA\Contact(
     *          email="admin@admin.com"
     *      ),
     *      @OA\License(
     *          name="Apache 2.0",
     *          url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *      )
     * )
     *
     * @OA\Server(
     *      url=L5_SWAGGER_CONST_HOST,
     *      description="Demo API Server"
     * )

     *
     * @OA\Tag(
     *     name="Projects",
     *     description="API Endpoints of Projects"
     * )
     */
}
